import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Contact } from '../shared/models/contact.model';
import { Router } from "@angular/router";
import { log } from 'util';

@Component({
  selector: 'app-editcontact',
  templateUrl: './editcontact.component.html',
  styleUrls: ['./editcontact.component.css']
})
export class EditcontactComponent implements OnInit {
  contact: any;
  contact1 = new Contact();
  updatedContactValue = new Contact();


  constructor(private rest: RestService, private router: Router ) { }

  ngOnInit() {
    this.contact;
    let userId = localStorage.getItem("editContactId");
    console.log(userId);
    this.rest.getContactById(+userId).subscribe((resp: any) => {
      console.log(resp.data);
      this.updatedContactValue = resp.data;
      console.log(this.contact1);
      
    });
  }

  update() {
    console.log(this.updatedContactValue);
    
    if (this.updatedContactValue != null) {
      this.rest.updateUser(this.updatedContactValue).subscribe((resp: any) => {
        console.log(resp);
        this.router.navigate(['dashboard']);
      })

    }

  }

  

}
