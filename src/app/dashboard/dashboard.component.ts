import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RestService} from '../rest.service';
import { Contact } from '../shared/models/contact.model';
import { Router } from "@angular/router";





@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = localStorage.getItem("loggedIndata");
  idloggedIn:any;
  name:any;
  contact: any = [];
  constructor(private rest: RestService, private router: Router) { }
  ngOnInit() {
    let ide = localStorage.getItem("loggedIndata");
    this.idloggedIn = localStorage.getItem("loggedInUserId");
    console.log(ide);

    if (ide == null) {
      console.log(ide);
      this.router.navigate(['']);


    }
    this.name = this.rest.getName();
    this.getProducts();
    
  }

  getProducts() {
    this.contact = [];
    this.rest.getProducts(this.idloggedIn).subscribe((resp:any) => {
      console.log(resp.data);
      this.contact = resp.data;
    });
  }

  deleteUser(contact: Contact): void{
    console.log(contact);
    alert('are you want to delet?');

    this.rest.deleteUser(contact.id).subscribe((resp: any) => {
      console.log(resp.data);

      this.getProducts();
    })
  };

  editUser(contact: Contact): void {

    localStorage.removeItem("editContactId");
    localStorage.setItem("editContactId", contact.id.toString());
    this.router.navigate(['editcontact']);
    //this.router.navigate()
    //this.rest.updateUser(contact).subscribe((resp: any) =>{
  };


  logoutUser(){
    localStorage.removeItem("loggedIndata");
    this.router.navigate(['']);

  }

  deleteAll(){

    this.rest.deleteAllCotact().subscribe((resp: any) => {
      console.log(resp.data);
      alert('are you want to delet?');
      this.getProducts();
    })

  };


  

}
