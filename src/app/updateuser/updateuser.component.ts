import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Contact } from '../shared/models/contact.model';
import { Router } from "@angular/router";
import { log } from 'util';
import { User } from '../shared/models/User.model'; 

@Component({
  selector: 'app-updateuser',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.css']
})
export class UpdateuserComponent implements OnInit {

  updatedUserDetails = new User();
  constructor(private rest: RestService, private router: Router ) { }

  ngOnInit() {
    let loggedInUserId = localStorage.getItem("loggedIndata");
    this.rest.getuserById(+loggedInUserId).subscribe((resp:any)=>{
      console.log(resp.data);
      this.updatedUserDetails = resp.data;
      console.log(this.updatedUserDetails);
    });
  }

  updateUser(){
    console.log(this.updatedUserDetails);
    this.rest.updateUserDetails(this.updatedUserDetails).subscribe((resp: any)=>{
      console.log(resp);
      this.router.navigate(['dashboard']);
    });


  }

}
