import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { User } from './shared/models/User.model';


import { HttpClientModule } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

import { Contact} from './shared/models/contact.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};


@Injectable()
export class RestService {
 // readonly baseUrl:"localhost:8080"
  constructor(private http: HttpClient) {
    

  }
  baseUrl: string = 'http://localhost:8080/contactserver/api';

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

///  getContact() {
 //   return this.http.get('localhost:8080/contactserver/api/contact/all')
  //    .map((res: Response) => res.json().response);
 // }

  getProducts(id: number): Observable<any> {
    
    let ide = localStorage.getItem("loggedIndata");
    httpOptions.headers.append('Access-Control-Allow-Origin', 'http://localhost:8080');
    httpOptions.headers.append('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    httpOptions.headers.append('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    httpOptions.headers.append('Access-Control-Allow-Credentials', 'true');

    return this.http.get(this.baseUrl + '/contact/all/' + ide, httpOptions).pipe(
      map(this.extractData));
  }
  
  postcontactData(object){
    let ide = localStorage.getItem("loggedIndata");
    return this.http.post('http://localhost:8080/contactserver/api/contact/' + ide +'/contact', object, httpOptions);
  }

  deleteUser(id: number) {
    return this.http.delete(this.baseUrl + '/contact/' + id, httpOptions);
  }

  updateUser(contact: Contact) {
    return this.http.put(this.baseUrl + '/contact/' + localStorage.getItem("editContactId"), contact);
  }

  getContactById(id: number){
    return this.http.get(this.baseUrl + '/contact/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  getuserById(id: number) {
    return this.http.get(this.baseUrl + '/users/' + id, httpOptions).pipe(
      map(this.extractData));
  }


  postUserData(user: User){
    return this.http.post('http://localhost:8080/contactserver/api/users', user, httpOptions);


  }

  loginUser(user: User) {
    return this.http.post('http://localhost:8080/contactserver/api/users/login', user, httpOptions);


  }

  name = localStorage.getItem("loggedIndata");

  deleteAllCotact(){
    return this.http.delete(this.baseUrl + '/contact/deleteall' , httpOptions);


  }

  updateUserDetails(userData: User) {
    return this.http.put(this.baseUrl + '/users/' + localStorage.getItem("loggedIndata"), userData);
  }



  loggedInName:any;

  setUsername(name: String){
    this.loggedInName = name;
  }

  getName(){
    return this.loggedInName;
  }


  
  
  
}
