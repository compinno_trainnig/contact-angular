import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Contact} from '../shared/models/contact.model';
import { Router } from "@angular/router";

@Component({
  selector: 'app-addcontact',
  templateUrl: './addcontact.component.html',
  styleUrls: ['./addcontact.component.css']
})
export class AddcontactComponent implements OnInit {
    object:any;
  //data = new Contact();
  success = true;
  namevalid:any;
  numberValid:any;
    
  constructor(public api:RestService, private router: Router) {
  
   }

  ngOnInit() {
    this.object={
      name: "",
      number: ""
    }
  }
  add(){
    console.log(this.object);
    if(this.object.name == null || this.object.name==""){
      this.namevalid='Name is required'
      this.success = false;
    }
    if (this.object.number == null || this.object.number == "") {
      this.numberValid = 'Number is required';
      this.success = false;
    }
    if (this.success){
      this.api.postcontactData(this.object).subscribe((resp: any) => {
        console.log(resp);
        this.router.navigate(['dashboard']);
      })

    }
   
  }

}
