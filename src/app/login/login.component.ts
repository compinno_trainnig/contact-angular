import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { User } from '../shared/models/User.model';
import { Router } from "@angular/router";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  data = new User();
  namevalid: any;
  emailValid: any;
  loginData = new User();
  loggedIndata = new User();
  constructor(public api: RestService, private router: Router) { }

  ngOnInit() {
    let ide = localStorage.getItem("loggedIndata");
    console.log(ide);
    
    if(ide!=null){
      console.log(ide);
      this.router.navigate(['dashboard']);


    }

  }

  createUser() {
    console.log(this.data);
    //if(this.data.name==null || this.data.name ==){

    //}
    if (this.data != null) {
      this.api.postUserData(this.data).subscribe((resp: any) => {
        console.log(resp);
        if(resp.messages =! null){
          alert('Email Already Exist');
        }
      }), error => {
        alert('Email Already Exist');
      }

    }
    this.router.navigate(['']);

  }

  loginUser(){
    console.log(this.loginData);
    
    this.api.loginUser(this.loginData).subscribe((resp: any) =>{
      console.log(resp);
      this.loggedIndata = resp.data;
      console.log(this.loggedIndata.name);
      this.namevalid = this.loggedIndata.name;
      if(resp.data.id!=null){
        //localStorage.setItem("loggedInUserId", resp.data.id);
        localStorage.setItem("loggedIndata", resp.data.id);
       let loggedInUserId  = localStorage.getItem("loggedIndata");
        console.log(loggedInUserId);
        this.api.setUsername(this.namevalid);
        this.router.navigate(['dashboard']);
      }

      //
      
    },error=>{
      alert('User Or Password is incorret');    }

    )
    

  }

}
