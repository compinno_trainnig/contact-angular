import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { RouterModule} from '@angular/router';
import  { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AddcontactComponent } from './addcontact/addcontact.component';
import { LoginComponent } from './login/login.component';
import { RestService } from './rest.service';
import { NgxPaginationModule } from 'ngx-pagination';
import { EditcontactComponent } from './editcontact/editcontact.component';
import { UpdateuserComponent } from './updateuser/updateuser.component';
import { ResetpwdComponent } from './resetpwd/resetpwd.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AddcontactComponent,
    LoginComponent,
    EditcontactComponent,
    UpdateuserComponent,
    ResetpwdComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot([
      {
        path: '',
        component: LoginComponent

      },
      {
        path: 'dashboard',
        component: DashboardComponent

      },
        {
        path: 'addcontact',
          component: AddcontactComponent

      },
      {
        path: 'editcontact',
        component: EditcontactComponent

      },
      {
        path: 'updateuser',
        component: UpdateuserComponent

      }

      
      
    ])
  ],
  providers: [RestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
